'use strict';
const express = require('express');
const app = express();
const request = require('request');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');
var http = require('http'); 

var configDB = require('./config/database.js');

//configuration ==================
require('./config/passport')(passport); // pass passport for configuration

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

//set up ejs for templating
app.set('view engine', 'ejs');

// const urlBase = `https://sandbox-api.lomadee.com/v2/14999560674164e1119b1/offer/_bestsellers?sourceId=35808026&page=${pagina}`;
// const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9';

// required for passport
app.use(session({ secret: 'ilovesplaytennis' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

/*########### ROUTES ###########*/

app.get('/auth/facebook', passport.authenticate('facebook', { scope : ['email'] }));

// /auth/facebook/callback: Facebook sends our user back to our application here with token and profile information.
// handle the callback after facebook has authenticated the user
app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/offers',
            failureRedirect : '/login'
        }));

app.get('/login', function(req, res) {
        console.log('Falha no login')
        res.send('Falha no login');
});

app.get('/saiuOffers', function(req, res){
    console.log('pegou ofertas');
    res.send('pegou ofertas');
});

app.get('/offers', function (req, res) {
  // var getOffers = function (req, res) {
  console.log('Autenticacao feita com sucesso')
  //Find the best offers from Lomadee API
  requestAllOffers();

  /*############ FUNCTIONS ############*/

console.log('entrou aqui getOffers');
  var arrayDiscountOffers = [];

  function requestAllOffers(pagina = 1, array = []) {
      var options = {
        url: `https://api.lomadee.com/v2/14999560674164e1119b1/offer/_bestsellers?sourceId=35811630&page=${pagina}`,
        method: 'GET',
        headers: {
          'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'
        }
      };
      request(options, function (error, response, body) {
        if (error || response.statusCode >= 400) {
          console.error('Could not return information!', error ? error : body);
        }

        let res_obj = JSON.parse(body);
        if (res_obj.pagination.page === res_obj.pagination.totalPage) {
          console.log('saiu');
          console.log(res_obj.pagination.page);

          //console.log(arrayDiscountOffers);
          console.log(arrayDiscountOffers.length);

          //call new request lomadee link
          generateLomadeezavelLink(arrayDiscountOffers);

        } else {
          
          array = res_obj.offers;
          
          console.log(res_obj.pagination.page);
         
          //create object and save info in an array
          array.filter((array_element) => {
            let obj_post = {
              offer_id: '',
              offer_link: '',
              offer_name: '',
              offer_good_img: '',
              offer_min_price: '',
              offer_max_price: '',
              offer_discount: ''
            };
            
            //console.log(array_element);
            if(array_element.discount >= 40){
              obj_post.offer_id = array_element.id;
              obj_post.offer_link = array_element.link;
              obj_post.offer_name = array_element.name;
              obj_post.offer_good_img = array_element.product.thumbnail.url;
              obj_post.offer_min_price = array_element.product.priceMin;
              obj_post.offer_max_price = array_element.product.priceMax;
              obj_post.offer_discount = array_element.discount;

              arrayDiscountOffers.push(obj_post);
              // console.log(arrayDiscountOffers);
              console.log(`preencheu array: ${JSON.stringify(obj_post)}`);
            } else {
              console.log(`Desconto menor que 40%`);
            }

          });

          requestAllOffers(pagina + 1, array);
        }

      });
    }; 

    function generateLomadeezavelLink(arrayDiscountOffersLomadezavel){
      console.log(`to lomadezate ${arrayDiscountOffersLomadezavel.length}`);
      var ii = 0;
      arrayDiscountOffersLomadezavel.forEach(function(array_discount_element) {
        // console.log(array_discount_element.offer_link);
        // console.log('ok');
        //let offer_link = array_discount_element.offer_link;
        var options = {
          url: `https://api.lomadee.com/v2/14999560674164e1119b1/deeplink/_create?sourceId=35808026&url=${array_discount_element.offer_link}`,
          method: 'GET',
          headers: {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'
          }
        };
        request(options, function (error, response, body) {
          if (error || response.statusCode >= 400) {
            console.error('Could not return information!', error ? error : body);
          } 

          //get url lomadezavel and together with the other info, post into whatsapp 
          let res_obj = JSON.parse(body);

          //console.log(response);
          //console.log(res_obj.deeplinks[0].message)
          arrayDiscountOffersLomadezavel[ii].offer_link_lomadezavel = res_obj.deeplinks[0].deeplink;
          
          //console.log(arrayDiscountOffersLomadezavel);
          //console.log(arrayDiscountOffers);
          ii++;

          if (ii === arrayDiscountOffersLomadezavel.length){
            console.log('saiu3');
            //console.log(arrayDiscountOffersLomadezavel);
            postFacebook(arrayDiscountOffersLomadezavel);
            //sendResultoToFrontend(arrayDiscountOffersLomadezavel);

          }

        });
      });

      console.log('saiu2');

    };

    // function sendResultoToFrontend(arrayReady){
    //   res.render('offers', {oferta: arrayReady});

    // };

    function postFacebook(arrayReady) {
        
        console.log(arrayReady);
        const DELAY_IN_MS = 60000; //60s, 180s
        var jj = 0;

        arrayReady.forEach((array_ready_element, index) => {
          const id = '523094484720894';  //page id
          const access_token = 'EAAbGojP9rKsBAFtZAsDJdXRcrcIHq8IoQ0xrGVnK8TZAZAz3vf4B3MELYcgZAh2drLRpE8sTiSNjjYaFjin3aAR8apLVj6M1bAwGpqO8J79DWbrzGFPrLbHlpmlAbCsLRdZAdZCveiTUz285NuOuN034L9dLCRTNtpcKsRFKUKUgZDZD'; //get from db - 'for page if posting to a page, for user if posting to a user\'s feed' 

          const postTextOptions = {  
            method: 'POST',
            uri: `https://graph.facebook.com/${id}/photos`,
            qs: {
              access_token: access_token,
              unpublished_content_type: 'DRAFT',
              published: false, 
              caption: `${array_ready_element.offer_name} com ${array_ready_element.offer_discount}% de desconto...não perca!!!!
              De: R$ ${array_ready_element.offer_max_price} Por: R$ ${array_ready_element.offer_min_price}
              ${array_ready_element.offer_link}`, //offer_link = link lomadezavel grande
              url: `${array_ready_element.offer_good_img}` //img 600 x 600
            }
          };

           

          // setTimeout(() => { delay time to post next offer commented
            // request(arrayReady[index]) request inside timeout
            request(postTextOptions, function(error, response, body) {                
              if (!error && response.statusCode == 200) {
                jj++;
                console.log(jj);
                console.log('msg x postada no feed');
                if(jj === arrayReady.length){
                  console.log('fim');
                  return
                } 
              } else {     
                console.log('process end with error');       
                return;
              }  
            });
          // }, index * DELAY_IN_MS);
        })
    };

    res.redirect(`/saiuOffers`);
//  };
 });

// app.get('/offers', getOffers);

app.listen(process.env.PORT || 4000)
console.log('Listening...');